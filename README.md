# Injecting a `javax.servlet.filter` into a Jersey app

This project is taken from https://github.com/awslabs/aws-serverless-java-container/tree/aws-serverless-java-container-1.3) and is licensed under the same terms as the original, the Apache 2.0 license.

This is a companion for https://www.jvt.me/posts/2021/01/25/servlet-filter-jersey-aws-serverless/
